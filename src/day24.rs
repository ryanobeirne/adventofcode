use adventofcode::*;
use std::collections::HashMap;

fn main() -> Result<()> {
    let mut grid = parse_input("a")?;

    answer!("a", grid.black_count());

    grid.run(100);

    answer!("b", grid.black_count());

    Ok(())
}

#[derive(Debug)]
struct Grid {
    map: HashMap<Point, Tile>,
}

impl Grid {
    fn black_count(&self) -> usize {
        self.map.values().filter(|tile| tile == &&Black).count()
    }

    fn run(&mut self, days: usize) {
        for _day in 0..days {
            self.daily_flip();
        }
    }

    fn daily_flip(&mut self) {
        let mut flips = Vec::new();
        let mut adds = Vec::<Adjacents>::new();

        for point in self.map.keys() {
            let adjacents = self.adjacents(point);
            adds.push(adjacents);
        }

        for adj in adds {
            self.insert_adjacents(adj);
        }

        for (point, tile) in self.map.iter() {
            let adjacents = self.adjacents(point);
            match tile {
                Black => {
                    let black_adj = adjacents.iter().filter(|tile| tile.1 == Black).count();
                    if black_adj == 0 || black_adj > 2 {
                        flips.push(point.clone());
                    }
                }
                White => {
                    if adjacents.iter().filter(|tile| tile.1 == Black).count() == 2 {
                        flips.push(point.clone());
                    }
                }
            }
        }

        for point in flips {
            self.map.entry(point).or_insert(White).flip()
        }
    }

    fn adjacents(&self, point: &Point) -> Adjacents {
        let nw = point.go(&NW);
        let ne = point.go(&NE);
        let e = point.go(&E);
        let se = point.go(&SE);
        let sw = point.go(&SW);
        let w = point.go(&W);

        [
            (nw, *self.map.get(&nw).unwrap_or(&White)),
            (ne, *self.map.get(&ne).unwrap_or(&White)),
            (e, *self.map.get(&e).unwrap_or(&White)),
            (se, *self.map.get(&se).unwrap_or(&White)),
            (sw, *self.map.get(&sw).unwrap_or(&White)),
            (w, *self.map.get(&w).unwrap_or(&White)),
        ]
    }

    fn insert_adjacents(&mut self, adjacents: Adjacents) {
        for (point, tile) in adjacents.iter() {
            self.map.insert(*point, *tile);
        }
    }
}

type Adjacents = [(Point, Tile); 6];

#[test]
fn example() -> Result<()> {
    let mut grid = parse_input("example")?;
    grid.run(100);
    answer!("example", grid.black_count());

    Ok(())
}

use Tile::*;
#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
enum Tile {
    White,
    Black,
}

impl Default for Tile {
    fn default() -> Self {
        White
    }
}

impl Tile {
    fn flip(&mut self) {
        match self {
            Black => *self = White,
            White => *self = Black,
        }
    }
}

#[derive(Debug)]
struct TilePath {
    path: Vec<Direction>,
}

impl TilePath {
    fn new() -> Self {
        TilePath {
            path: Vec::new(),
        }
    }

    fn iter(&self) -> TilePathIter {
        TilePathIter {
            path: self.path.iter().collect(),
            loc: Point::new(0,0),
            index: 0,
        }
    }

    fn push(&mut self, dir: Direction) {
        self.path.push(dir)
    }
}

#[derive(Debug)]
struct TilePathIter<'a> {
    path: Vec<&'a Direction>,
    loc: Point,
    index: usize,
}

impl<'a> Iterator for TilePathIter<'a> {
    type Item = Point;
    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.path.len() {
            None
        } else {
            self.loc = self.loc.go(self.path[self.index]);
            self.index += 1;
            Some(self.loc)
        }
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(x: i32, y: i32) -> Self {
        Point {x, y}
    }

    fn go(&self, dir: &Direction) -> Self {
        match dir {
            E => Point::new(self.x + 2, self.y),
            W => Point::new(self.x - 2, self.y),
            NE => Point::new(self.x + 1, self.y - 1),
            NW => Point::new(self.x - 1, self.y - 1),
            SE => Point::new(self.x + 1, self.y + 1),
            SW => Point::new(self.x - 1, self.y + 1),
        }
    }
}

use Direction::*;
#[derive(Debug)]
enum Direction {
    NW,
    NE,
    E,
    SE,
    SW,
    W,
}

impl FromStr for Direction {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s.to_lowercase().trim() {
            "nw" => NW,
            "ne" => NE,
            "e" => E,
            "se" => SE,
            "sw" => SW,
            "w" => W,
            x => return err!("invalid direction: `{}`", x),
        })
    }
}

fn parse_input(file: &str) -> Result<Grid> {
    let mut map = HashMap::new();

    for line in input!(file).lines() {
        let mut path = TilePath::new();
        let line = line?;
        let mut chars = line.chars();

        while let Some(first) = chars.next() {
            match first {
                'n' => match chars.next().unwrap() {
                    'e' => path.push(NE),
                    'w' => path.push(NW),
                    x => return err!("invalid character: `{}`", x),
                }
                's' => match chars.next().unwrap() {
                    'e' => path.push(SE),
                    'w' => path.push(SW),
                    x => return err!("invalid character: `{}`", x),
                }
                'e' => path.push(E),
                'w' => path.push(W),
                x => return err!("invalid character: `{}`", x),
            }
        }

        if let Some(point) = path.iter().last() {
            map.entry(point).or_insert(White).flip();
        }
    }

    Ok(Grid { map })
}
