use adventofcode::*;
use std::collections::HashMap;

fn main() -> Result<()> {
    let notes = parse_input("a")?;
    let next = notes.next_departure();

    answer!("a", notes.answer_a(next.0, next.1));

    Ok(())
}

#[derive(Debug)]
struct Notes {
    earliest: usize,
    busses: Vec<Bus>,
}

impl Notes {
    fn next_departure(&self) -> (&Bus, usize) {
        let mut departures = HashMap::<&Bus, usize>::new();

        for bus in self.busses.iter() {
            departures.insert(bus, bus.iter().at(self.earliest).next().unwrap());
        }

        departures.into_iter()
            .min_by(|(_, val_a), (_, val_b)| val_a.cmp(val_b))
            .unwrap()
    }

    fn answer_a(&self, bus: &Bus, departure: usize) -> usize {
        bus.0 * (departure - self.earliest)
    }
}

#[derive(Debug)]
struct BusIter {
    id: usize,
    index: usize,
}

impl BusIter {
    fn at(self, index: usize) -> Self {
        BusIter {
            id: self.id,
            index: (index - index % self.id) + self.id,
        }
    }
}

impl Iterator for BusIter {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        self.index += self.id;
        Some(self.index - self.id)
    }
}

#[test]
fn bus_iter() {
    let mut bus_iter = BusIter { id: 5, index: 0 };
    assert_eq!(bus_iter.next(), Some(0));
    assert_eq!(bus_iter.next(), Some(5));
    assert_eq!(bus_iter.next(), Some(10));
    assert_eq!(bus_iter.next(), Some(15));

    bus_iter = bus_iter.at(69);
    assert_eq!(bus_iter.next(), Some(70));

    bus_iter = bus_iter.at(93);
    assert_eq!(bus_iter.next(), Some(95));
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Bus(usize);

impl Bus {
    fn iter(&self) -> BusIter {
        BusIter {
            id: self.0,
            index: 0,
        }
    }
}

fn parse_input(file: &str) -> Result<Notes> {
    let mut lines = input!(file).lines();
    let earliest = lines.nth(0).ok_or("empty earliest")??.parse()?;

    let busses = lines.nth(0)
        .ok_or("empty busses")??
        .split(',')
        .filter_map(|id| Some(Bus(id.parse::<usize>().ok()?)))
        .collect();

    Ok(Notes { earliest, busses })
}
