use adventofcode::*;
use std::collections::HashMap;
use std::iter::repeat;

fn main() -> Result<()> {
    let mut seating_chart = parse_lines("a")?;
    seating_chart.run();
    answer!("a", seating_chart.occupied());

    let mut seating_chart = parse_lines("a")?;
    seating_chart.runv2();
    answer!("b", seating_chart.occupied());

    Ok(())
}

#[derive(Debug, Clone)]
struct SeatingChart {
    seats: HashMap<Seat, State>,
}

impl SeatingChart {
    fn run(&mut self) -> usize {
        let mut steps = 0;
        while self.step() {
            steps += 1;
        }
        steps
    }

    fn runv2(&mut self) -> usize {
        let mut steps = 0;
        while self.stepv2() {
            steps += 1;
            dbg!(&steps);
        }
        steps
    }

    /// Returns false if the seating chart has not changed after one step
    fn step(&mut self) -> bool {
        let mut switches = Vec::<Seat>::new();

        for (seat, state) in self.seats.iter() {
            match state {
                Empty => {
                    if self.adjacent_count(seat) == 0 {
                        switches.push(*seat);
                    }
                }
                Occupied => {
                    if self.adjacent_count(seat) > 3 {
                        switches.push(*seat);
                    }
                }
                Floor => ()
            }
        }

        if switches.is_empty() {
            false
        } else {
            for seat in switches {
                self.seats
                    .get_mut(&seat)
                    .expect("we checked this already")
                    .switch();
            }
            true
        }
    }

    /// Returns false if the seating chart has not changed after one step
    fn stepv2(&mut self) -> bool {
        let mut switches = Vec::<Seat>::new();

        for (seat, state) in self.seats.iter() {
            match state {
                Empty => {
                    if self.vector_count(seat) == 0 {
                        switches.push(*seat);
                    }
                }
                Occupied => {
                    if self.vector_count(seat) > 4 {
                        switches.push(*seat);
                    }
                }
                Floor => ()
            }
        }

        if switches.is_empty() {
            false
        } else {
            for seat in switches {
                self.seats
                    .get_mut(&seat)
                    .expect("we checked this already")
                    .switch();
            }
            true
        }
    }

    fn occupied(&self) -> usize {
        self.seats.values().filter(|state| state == &&Occupied).count()
    }

    fn adjacent_count(&self, seat: &Seat) -> usize {
        self.adjacent_seats(seat).iter().filter(|seat| {
            self.seats.get(seat) == Some(&Occupied)
        })
        .count()
    }

    fn adjacent_seats(&self, seat: &Seat) -> [Seat; 8] {
        [
            Seat::new(seat.row - 1, seat.col - 1),
            Seat::new(seat.row - 1, seat.col),
            Seat::new(seat.row - 1, seat.col + 1),
            Seat::new(seat.row, seat.col - 1),
            Seat::new(seat.row, seat.col + 1),
            Seat::new(seat.row + 1, seat.col - 1),
            Seat::new(seat.row + 1, seat.col),
            Seat::new(seat.row + 1, seat.col + 1),
        ]
    }

    fn vector_count(&self, seat: &Seat) -> usize {
        CARDINAL_DIRS.iter()
            .filter_map(|dir| self.vector_view(seat, dir))
            .filter(|state| state == &&Occupied)
            .count()
    }

    fn vector_view(&self, seat: &Seat, direction: &Cardinal) -> Option<&State> {
        self.seat_vec(seat, direction).find_map(|seat| {
            let get = self.seats.get(&seat);
            if get == Some(&Occupied) || get == Some(&Empty) {
                get
            } else {
                if let None = get {
                    panic!("This shouldn't be possible");
                }
                None
            }
        })
    }

    fn seat_vec(&self, seat: &Seat, direction: &Cardinal) -> SeatVec {
        let (max_row, max_col) = (self.max_rows(), self.max_cols());

        let iter: Vec<_> = match direction {
            NW=> (0..seat.row).rev().zip((0..seat.col).rev()).collect(),
            N => (0..seat.row).rev().zip(repeat(seat.col)).collect(),
            NE=> (0..seat.row).rev().zip(seat.col+1..=max_col).collect(),
            W => repeat(seat.row).zip((0..seat.col).rev()).collect(),
            E => repeat(seat.row).zip(seat.col+1..=max_col).collect(),
            SW=> (seat.row+1..=max_row).zip((0..seat.col).rev()).collect(),
            S => (seat.row+1..=max_row).zip(repeat(seat.col)).collect(),
            SE=> (seat.row+1..=max_row).zip(seat.col+1..=max_col).collect(),
        };

        SeatVec {
            seats: iter.into_iter().map(|(row, col)| Seat::new(row, col)).collect(),
            index: 0
        }
    }

    fn max_rows(&self) -> i32 {
        self.seats
            .keys()
            .max_by(|seat_a, seat_b| seat_a.row.cmp(&seat_b.row))
            .unwrap_or(&Seat::new(0,0))
            .row
    }

    fn max_cols(&self) -> i32 {
        self.seats
            .keys()
            .max_by(|seat_a, seat_b| seat_a.col.cmp(&seat_b.col))
            .unwrap_or(&Seat::new(0,0))
            .col
    }
}

#[test]
fn example1() -> Result<()> {
    let mut seating_chart = parse_lines("example1")?;
    seating_chart.run();
    println!("{}", &seating_chart);

    assert_eq!(seating_chart.occupied(), 37);

    Ok(())
}

#[test]
fn example2() -> Result<()> {
    let mut seating_chart = parse_lines("example1")?;
    seating_chart.runv2();
    eprintln!("{}", &seating_chart);

    assert_eq!(seating_chart.occupied(), 26);

    Ok(())
}

#[test]
fn example2a() -> Result<()> {
    let seating_chart = parse_lines("example2a")?;
    eprintln!("{}", seating_chart);
    assert_eq!(seating_chart.seats.get(&Seat::new(4,3)), Some(&Empty));
    assert_eq!(seating_chart.vector_count(&Seat::new(4, 3)), 8);
    Ok(())
}

#[test]
fn example2b() -> Result<()> {
    let seating_chart = parse_lines("example2b")?;
    assert_eq!(seating_chart.seats.get(&Seat::new(1,1)), Some(&Empty));
    assert_eq!(seating_chart.vector_count(&Seat::new(1,1)), 0);
    Ok(())
}

struct SeatVec {
    seats: Vec<Seat>,
    index: usize,
}

impl Iterator for SeatVec {
    type Item = Seat;
    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.seats.len() {
            None
        } else {
            self.index += 1;
            Some(self.seats[self.index - 1])
        }
    }
}

const CARDINAL_DIRS: &[Cardinal] = &[NW, N, NE, W, E, SW, S, SE];

use Cardinal::*;
#[derive(Debug, PartialEq, Eq)]
enum Cardinal {
    NW,
    N,
    NE,
    W,
    E,
    SW,
    S,
    SE,
}

use std::fmt::Write;
impl fmt::Display for SeatingChart {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in 0..=self.max_rows() {
            for col in 0..=self.max_cols() {
                f.write_char(char::from(self.seats.get(&Seat::new(row, col)).unwrap_or_default()))?;
            }
            f.write_char('\n')?;
        }

        Ok(())
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, PartialOrd, Eq, Ord)]
struct Seat {
    row: i32,
    col: i32,
}

impl Seat {
    fn new(row: i32, col: i32) -> Self {
        Seat { row, col }
    }
}

use State::*;
#[derive(Debug, Clone, Hash, PartialEq, PartialOrd, Eq, Ord)]
enum State {
    Empty,
    Occupied,
    Floor,
}

impl State {
    fn switch(&mut self) {
        match self {
            Empty => *self = Occupied,
            Occupied => *self = Empty,
            Floor => (),
        }
    }
}

impl Default for &State {
    fn default() -> Self {
        &Floor
    }
}

impl From<&State> for char {
    fn from(state: &State) -> char {
        match state {
            Empty => 'L',
            Occupied => '#',
            Floor => '.',
        }
    }
}

impl TryFrom<char> for State {
    type Error = BoxErr;
    fn try_from(s: char) -> Result<Self> {
        Ok(match s {
            'L' => Empty,
            '#' => Occupied,
            '.' => Floor,
            x => return err!("invalid state: {}", x),
        })
    }
}

fn parse_lines(file: &str) -> Result<SeatingChart> {
    let mut seats = HashMap::new();

    for (row, line) in input!(file).lines().enumerate() {
        for (col, state) in line?.chars().enumerate() {
            seats.insert(Seat::new(row as i32, col as i32), State::try_from(state)?);
        }
    }

    Ok(SeatingChart{seats})
}
