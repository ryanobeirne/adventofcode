use adventofcode::*;

fn main() -> Result<()> {
    let ids = input!("a")
        .lines()
        .filter_map(parse::<Directions>)
        .map(Seat::from)
        .map(|seat| seat.id())
        .collect::<Vec<_>>();

    let max = ids.iter().max().ok_or(error!("empty seats"))?;

    answer!("a", max);

    let min = ids.iter().min().ok_or(error!("empty seats"))?;

    for i in *min..*max {
        if !ids.contains(&i) {
            answer!("b", i);
        }
    }

    Ok(())
}

#[derive(Debug, PartialEq, Eq)]
struct Seat {
    row: Row,
    column: Column,
}

impl Seat {
    fn id(&self) -> u32 {
        self.row.0 as u32 * 8 + self.column.0 as u32
    }
}

impl From<Directions> for Seat {
    fn from(dir: Directions) -> Self {
        Seat {
            row: Row::from(dir.row),
            column: Column::from(dir.col),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
struct Row(u8);

impl From<RowDirections> for Row {
    fn from(row_dirs: RowDirections) -> Self {
        let mut range = 0..128_u8;

        for dir in row_dirs.0.iter() {
            match dir {
                Front => range.end -= (range.end - range.start) / 2,
                Back => range.start += (range.end - range.start) / 2,
            }
        }

        Row(range.start)
    }
}

#[test]
fn row_from_dir() -> Result<()> {
    // BFFFBBFRRR: row 70, column 7, seat ID 567
    let dir = "BFFFBBF".parse::<RowDirections>()?;
    assert_eq!(Row::from(dir), Row(70));
    Ok(())
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
struct Column(u8);

impl From<ColumnDirections> for Column {
    fn from(col_dirs: ColumnDirections) -> Self {
        let mut range = 0..8_u8;

        for dir in col_dirs.0.iter() {
            match dir {
                Left => range.end -= (range.end - range.start) / 2,
                Right => range.start += (range.end - range.start) / 2,
            }
        }

        Column(range.start)
    }
}

#[test]
fn col_from_dir() -> Result<()> {
    // BFFFBBFRRR: row 70, column 7, seat ID 567
    let dir = "RRR".parse::<ColumnDirections>()?;
    assert_eq!(Column::from(dir), Column(7));
    Ok(())
}

#[derive(Debug)]
struct Directions {
    row: RowDirections,
    col: ColumnDirections,
}

impl FromStr for Directions {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(Directions {
            row: s.get(0..7).ok_or(error!("Directions cannot be empty: `{}`", s))?.parse()?,
            col: s.get(7..10).ok_or(error!("Directions must have 10: `{}`", s))?.parse()?,
        })
    }
}

#[test]
fn directions_from_str() -> Result<()> {
    // BFFFBBFRRR: row 70, column 7, seat ID 567
    let dir = "BFFFBBFRRR".parse::<Directions>()?;
    let seat = Seat::from(dir);
    assert_eq!(seat, Seat { row: Row(70), column: Column(7) });
    assert_eq!(seat.id(), 567);
    Ok(())
}

#[derive(Debug)]
struct RowDirections([FrontBack; 7]);

impl FromStr for RowDirections {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(RowDirections([
            s.get(0..1).ok_or(error!("RowDirections cannot be empty: `{}`", s))?.parse()?,
            s.get(1..2).ok_or(error!("RowDirections must have 7: `{}`", s))?.parse()?,
            s.get(2..3).ok_or(error!("RowDirections must have 7: `{}`", s))?.parse()?,
            s.get(3..4).ok_or(error!("RowDirections must have 7: `{}`", s))?.parse()?,
            s.get(4..5).ok_or(error!("RowDirections must have 7: `{}`", s))?.parse()?,
            s.get(5..6).ok_or(error!("RowDirections must have 7: `{}`", s))?.parse()?,
            s.get(6..7).ok_or(error!("RowDirections must have 7: `{}`", s))?.parse()?,
        ]))
    }
}

#[derive(Debug)]
struct ColumnDirections([LeftRight; 3]);

impl FromStr for ColumnDirections {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(ColumnDirections([
            s.get(0..1).ok_or(error!("ColumnDirections cannot be empty: `{}`", s))?.parse()?,
            s.get(1..2).ok_or(error!("ColumnDirections must have 3: `{}`", s))?.parse()?,
            s.get(2..3).ok_or(error!("ColumnDirections must have 3: `{}`", s))?.parse()?,
        ]))
    }
}

use FrontBack::*;
#[derive(Debug)]
enum FrontBack {
    Front,
    Back,
}

impl FromStr for FrontBack {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s {
            "F" => Front,
            "B" => Back,
            x => return err!("invalid FrontBack character: `{}`", x),
        })
    }
}

use LeftRight::*;
#[derive(Debug)]
enum LeftRight {
    Left,
    Right,
}

impl FromStr for LeftRight {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s {
            "L" => Left,
            "R" => Right,
            x => return err!("invalid LeftRight character: `{}`", x),
        })
    }
}

