use adventofcode::*;
mod day12part2;
use day12part2::*;

fn main() -> Result<()> {
    let last = parse_input("a")?.last().ok_or("you did it wrong, dummy")?;

    answer!("a", last.manhattan(&Point::default()));
    part2()?;

    Ok(())
}

#[derive(Debug)]
pub struct Instructions {
    instructions: Vec<Instruction>,
    index: usize,
    location: Point,
    orientation: Cardinal,
}

impl Iterator for Instructions {
    type Item = Point;
    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.instructions.len() {
            None
        } else {
            let Instructions { location, orientation, .. } = self;
            *location = location.next(orientation, &self.instructions[self.index]);
            self.index += 1;
            Some(*location)
        }
    }
}

#[derive(Debug, PartialEq, Eq, Default, Copy, Clone)]
pub struct Point {
    row: i32,
    col: i32,
}

impl Point {
    fn new(row: i32, col: i32) -> Self {
        Point {row, col}
    }

    fn next(&self, orientation: &mut Cardinal, instruction: &Instruction) -> Self {
        match instruction {
            Move(card, dist) => {
                match card {
                    N => Point { row: self.row - dist, col: self.col },
                    S => Point { row: self.row + dist, col: self.col },
                    E => Point { row: self.row, col: self.col + dist },
                    W => Point { row: self.row, col: self.col - dist },
                }
            }
            Go(dist) => {
                self.next(orientation, &Move(*orientation, *dist))
            }
            Rotate(turn, degrees) => {
                *orientation = orientation.turn(turn, degrees);
                *self
            }
        }
    }

    pub fn manhattan(&self, other: &Self) -> i32 {
        (other.row - self.row).abs() + (other.col - self.col).abs()
    }
}

#[test]
fn next_point() -> Result<()> {
    let point = Point::default();
    let orientation = &mut E;
    let instruction = Move(E, 3);

    let next_point = point.next(orientation, &instruction);

    assert_eq!(next_point, Point { row: 0, col: 3});
    assert_eq!(orientation, &mut E);

    let action = Rotate(Right, Ninety);
    let next_point = next_point.next(orientation, &action);

    assert_eq!(next_point, Point { row: 0, col: 3 });
    assert_eq!(orientation, &mut S);

    let action = Go(8);
    let next_point = next_point.next(orientation, &action);
    assert_eq!(next_point, Point { row: 8, col: 3 });

    Ok(())
}

pub use Cardinal::*;
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Cardinal {
    N,
    S,
    E,
    W,
}

impl Cardinal {
    fn turn(&self, turn: &Turn, degrees: &Degrees) -> Self {
        match self {
            N => match turn {
                Left => match degrees {
                    Ninety => W,
                    OneEighty => S,
                    TwoSeventy => E,
                }
                Right => match degrees {
                    Ninety => E,
                    OneEighty => S,
                    TwoSeventy => W,
                }
            }
            S => match turn {
                Left => match degrees {
                    Ninety => E,
                    OneEighty => N,
                    TwoSeventy => W,
                }
                Right => match degrees {
                    Ninety => W,
                    OneEighty => N,
                    TwoSeventy => E,
                }
            }
            E => match turn {
                Left => match degrees {
                    Ninety => N,
                    OneEighty => W,
                    TwoSeventy => S,
                }
                Right => match degrees {
                    Ninety => S,
                    OneEighty => W,
                    TwoSeventy => N,
                }
            }
            W => match turn {
                Left => match degrees {
                    Ninety => S,
                    OneEighty => E,
                    TwoSeventy => N,
                }
                Right => match degrees {
                    Ninety => N,
                    OneEighty => E,
                    TwoSeventy => S,
                }
            }
        }
    }
}

pub use Turn::*;
#[derive(Debug, Copy, Clone)]
pub enum Turn {
    Right,
    Left,
}


pub use Degrees::*;
#[derive(Debug, Copy, Clone)]
pub enum Degrees {
    Ninety = 90,
    OneEighty = 180,
    TwoSeventy = 270,
}

impl TryFrom<i32> for Degrees {
    type Error = BoxErr;
    fn try_from(i: i32) -> Result<Self> {
        Ok(match i {
            90 => Ninety,
            180 => OneEighty,
            270 => TwoSeventy,
            x => return err!("invalid degrees: {}", x),
        })
    }
}

pub use Instruction::*;
#[derive(Debug, Copy, Clone)]
pub enum Instruction {
    Move(Cardinal, i32),
    Go(i32),
    Rotate(Turn, Degrees),
}

impl FromStr for Instruction {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let first = s.get(0..1).ok_or("empty instruction")?;
        let second = s.get(1..).ok_or("empty instruction value")?.parse::<i32>()?;
        Ok(match first {
            "N" => Move(N, second),
            "S" => Move(S, second),
            "E" => Move(E, second),
            "W" => Move(W, second),
            "F" => Go(second),
            "R" => Rotate(Right, second.try_into()?),
            "L" => Rotate(Left, second.try_into()?),
            x => return err!("invalid instruction: {}", x),
        })
    }
}

pub fn parse_input(file: &str) -> Result<Instructions> {
    let mut instructions = Vec::new();

    for line in input!(file).lines() {
        instructions.push(line?.parse::<Instruction>()?);
    }

    Ok(Instructions {
        instructions,
        index: 0,
        location: Point::default(),
        orientation: E,
    })
}
