use adventofcode::*;
use std::collections::HashMap;
use std::iter::once;

fn main() -> Result<()> {
    let adapters = parse_lines("a")?;

    if let Some((ones, threes)) = adapters.ones_and_threes() {
        answer!("a", ones as u32 * threes as u32);
        answer!("b", adapters.arrangement_count().ok_or("empty adapter bag")?);
    } else {
        return err!("you did it wrong, doofus");
    }

    Ok(())
}

#[derive(Debug)]
struct AdapterBag {
    adapters: Vec<u8>,
    joltage: u8,
}

#[derive(Debug)]
struct Gaps {
    gaps: Vec<u8>,
}

#[derive(Debug)]
struct DiffIter {
    diffs: Vec<u8>,
    index: usize,
}

impl Iterator for DiffIter {
    type Item = u8;
    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.diffs.len() {
            None
        } else {
            self.index += 1;
            Some(self.diffs[self.index - 1])
        }
    }
}

impl AdapterBag {
    fn arrangement_count(&self) -> Option<usize> {
        let mut sorted = self.sorted();
        todo!()
    }

    fn sorted(&self) -> Vec<&u8> {
        let mut sorted: Vec<&u8> = once(&0)
            .chain(self.adapters.iter())
            .chain(once(self.joltage()))
            .collect();

        sorted.sort();

        sorted
    }

    fn joltage(&self) -> &u8 {
        &self.joltage
    }

    fn diff_iter(&self) -> DiffIter {
        let mut diffs = Vec::new();
        
        let adapters = self.sorted();

        let mut peekable = adapters.iter().peekable();
        while let (Some(next), Some(peek)) = (peekable.next(), peekable.peek()) {
            diffs.push(**peek - **next);
        }

        DiffIter { diffs, index: 0 }
    }

    fn ones_and_threes(&self) -> Option<(u8, u8)> {
        let mut count = HashMap::new();

        for diff in self.diff_iter() {
            *count.entry(diff).or_insert(0) += 1;
        }

        Some((*count.get(&1)?, *count.get(&3)?))
    }

    fn is_valid(&self) -> bool {
        !self.diff_iter().any(|gap| gap > 3)
    }
}

fn parse_lines(input: &str) -> Result<AdapterBag> {
    let adapters = input!(input)
        .lines()
        .try_fold::<_, _, Result<Vec<u8>>>(Vec::new(), |mut vec, line| {
            vec.push(line?.parse::<u8>()?);
            Ok(vec)
        })?;

    let joltage = adapters.iter().max().ok_or("bag is empty")? + 3;

    Ok(AdapterBag { adapters, joltage })

}

#[test]
fn diff_count() -> Result<()> {
    let adapters = parse_lines("example2")?;
    assert_eq!(adapters.ones_and_threes(), Some((22, 10)));

    Ok(())
}

#[test]
fn arrangements() -> Result<()> {
    let adapters = parse_lines("example1")?;
    let (ones, threes) = adapters.ones_and_threes().ok_or("empty adapter bag")?;

    dbg!(ones, threes, ones * threes, &adapters, adapters.adapters.len(), adapters.joltage());
    
    assert_eq!(adapters.arrangement_count(), Some(8));

    Ok(())
}
