use adventofcode::*;
use std::collections::HashMap;

mod day17part2;
use day17part2::*;

fn main() -> Result<()> {
    let mut pocket = parse_input("a")?;
    pocket.run(6);
    answer!("a", pocket.active_count());

    let mut pocket4d = parse_input_4d("a")?;
    pocket4d.run(6);
    answer!("b", pocket4d.active_count());

    Ok(())
}

#[derive(Debug)]
struct Pocket {
    grid: HashMap<Point, State>,
}

impl Pocket {
    fn run(&mut self, cycles: usize) {
        for _ in 0..cycles {
            self.cycle();
        }
    }

    fn cycle(&mut self) {
        let Point { x: min_x, y: min_y, z: min_z } = self.min();
        let Point { x: max_x, y: max_y, z: max_z } = self.max();

        let mut flips = Vec::new();

        for x in min_x..=max_x {
            for y in min_y..=max_y {
                for z in min_z..=max_z {
                    let point = Point::new(x, y, z);
                    let neighbors = point.neighbors();
                    let active_count = neighbors.iter().filter(|n| self.grid.get(&n) == Some(&Active)).count();
                    match (self.grid.get(&point).unwrap_or_default(), active_count) {
                        (Active, 2) | (Active, 3) => (),
                        (Active, _) => {
                            flips.push(point);
                        }
                        (Inactive, 3) => {
                            flips.push(point);
                        }
                        _ => (),
                    }
                }
            }
        }

        for flip in flips {
            self.grid.entry(flip).or_default().flip();
        }
    }

    fn active_count(&self) -> usize {
        self.grid.values().filter(|state| state == &&Active).count()
    }

    fn min(&self) -> Point {
        Point::new(
            self.grid.keys().min_by(|a, b| a.x.cmp(&b.x)).unwrap().x - 1,
            self.grid.keys().min_by(|a, b| a.y.cmp(&b.y)).unwrap().y - 1,
            self.grid.keys().min_by(|a, b| a.z.cmp(&b.z)).unwrap().z - 1,
        )
    }

    fn max(&self) -> Point {
        Point::new(
            self.grid.keys().max_by(|a, b| a.x.cmp(&b.x)).unwrap().x + 1,
            self.grid.keys().max_by(|a, b| a.y.cmp(&b.y)).unwrap().y + 1,
            self.grid.keys().max_by(|a, b| a.z.cmp(&b.z)).unwrap().z + 1,
        )
    }
}

impl FromStr for Pocket {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut grid = HashMap::new();

        for (y, line) in s.lines().enumerate() {
            for (x, ch) in line.chars().enumerate() {
                grid.insert(
                    Point::new(x as i32, y as i32, 0),
                    ch.to_string().parse()?,
                );
            }
        }

        Ok(Pocket { grid })
    }
}

use State::*;
#[derive(Debug, PartialEq, Eq, Hash)]
enum State {
    Inactive,
    Active,
}

impl State {
    fn flip(&mut self) {
        match self {
            Inactive => *self = Active,
            Active => *self = Inactive,
        }
    }
}

impl Default for State {
    fn default() -> Self {
        Inactive
    }
}

impl<'a> Default for &'a State {
    fn default() -> Self {
        &Inactive
    }
}

impl FromStr for State {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s {
            "." => Inactive,
            "#" => Active,
            x => return err!("invalid State: `{}`", x),
        })
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
    z: i32,
}

impl Point {
    fn new(x: i32, y: i32, z: i32) -> Self {
        Point {x, y, z}
    }

    fn neighbors(&self) -> Vec<Self> {
        let mut neighbors = Vec::new();

        let x_p = self.x + 1;
        let x_n = self.x - 1;
        let y_p = self.y + 1;
        let y_n = self.y - 1;
        let z_p = self.z + 1;
        let z_n = self.z - 1;

        for x in x_n..=x_p {
            for y in y_n..=y_p {
                for z in z_n..=z_p {
                    let point = Point::new(x, y, z);
                    if &point != self {
                        neighbors.push(point);
                    }
                }
            }
        }

        neighbors
    }
}

fn parse_input(file: &str) -> Result<Pocket> {
    let input = &mut String::new();
    input!(file).read_to_string(input)?;
    input.parse()
}
