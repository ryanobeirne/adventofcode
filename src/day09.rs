use adventofcode::*;

fn main() -> Result<()> {
    let xmas = input!("a").lines().try_fold::<_, _, Result<Xmas>>(
        Xmas::new(),
        |mut xmas, line| {
            xmas.values.push(line?.parse()?);
            Ok(xmas)
        },
    )?;

    if let Some(nosum) = xmas.run() {
        answer!("a", nosum);
        answer!("b", xmas.weakness(&nosum).ok_or("you did it wrong, doofus")?);
    } else {
        return err!("you did it wrong, doofus");
    }

    Ok(())
}

#[derive(Debug, Default)]
struct Xmas {
    values: Vec<u64>,
}

impl Xmas {
    fn new() -> Self {
        Self::default()
    }

    fn run(&self) -> Option<u64> {
        let mut iter = self.iter();

        while let Some(slice) = iter.next() {
            let current = iter.current()?;

            if !slice_can_sum(slice, current) {
                return Some(*current);
            }
        }

        None
    }

    fn iter(&self) -> XmasIter {
        XmasIter {
            values: &self.values,
            start: 0,
            current: 24,
        }
    }

    fn weakness(&self, invalid: &u64) -> Option<u64> {
        for (i, _value) in self.values.iter().take_while(|val| invalid > val).enumerate() {
            for j in 0..i {
                for k in j..i {
                    let slice = self.values.get(j..k)?;
                    if slice_eq_sum(slice, invalid) {
                        return Some(slice.iter().max()? + slice.iter().min()?);
                    }
                }

            } 
        }
        None
    }
}

fn slice_eq_sum(slice: &[u64], sum: &u64) -> bool {
    &slice.iter().sum::<u64>() == sum
}

fn slice_can_sum(slice: &[u64], sum: &u64) -> bool {
    for i in slice.iter() {
        for j in slice.iter() {
            if &(i + j) == sum {
                return true;
            }
        }
    }

    false
}

#[derive(Debug, Clone, Copy)]
struct XmasIter<'a> {
    values: &'a Vec<u64>,
    start: usize,
    current: usize,
}

impl<'a> Iterator for XmasIter<'a> {
    type Item = &'a [u64];
    fn next(&mut self) -> Option<&'a [u64]> {
        let get = self.values.get(self.start..=self.current);
        self.start += 1;
        self.current += 1;
        get
    }
}

impl<'a> XmasIter<'a> {
    fn current(&self) -> Option<&u64> {
        self.values.get(self.current)
    }
}

impl std::iter::FromIterator<u64> for Xmas {
    fn from_iter<I: IntoIterator<Item=u64>>(iter: I) -> Self {
        Xmas {
            values: iter.into_iter().collect(),
        }
    }
}
