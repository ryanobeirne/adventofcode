use super::*;

pub fn part2() -> Result<()> {
    let mut instructions = parse_input2("a")?;
    instructions.run();
    answer!("b", instructions.location.manhattan(&Point::default()));

    Ok(())
}

#[derive(Debug)]
struct Instructions2 {
    instructions: Vec<Instruction>,
    index: usize,
    location: Point,
    waypoint: Point,
}

impl Instructions2 {
    // Returns the ship's location until we reach the end of the instructions
    fn step(&mut self) -> Option<Point> {
        if self.index >= self.instructions.len() {
            None
        } else {
            self.instruct(self.instructions[self.index]);
            self.index += 1;
            Some(self.location)
        }
    }

    fn instruct(&mut self, instruction: Instruction) {
        match instruction {
            Move(cardinal, dist) => {
                match cardinal {
                    N => self.waypoint.row -= dist,
                    S => self.waypoint.row += dist,
                    E => self.waypoint.col += dist,
                    W => self.waypoint.col -= dist,
                }
            }
            Go(dist) => {
                self.location.row += dist * self.waypoint.row;
                self.location.col += dist * self.waypoint.col;
            }
            Rotate(turn, degrees) => {
                self.waypoint = self.waypoint.rotate(turn, degrees);
            }
        }
    }

    fn run(&mut self) {
        while let Some(_point) = self.step() {}
    }
}

impl Point {
    fn rotate(&self, turn: Turn, degrees: Degrees) -> Self {
        match turn {
            Left => match degrees {
                Ninety => Point::new(-self.col, self.row),
                OneEighty => Point::new(-self.row, -self.col),
                TwoSeventy => Point::new(self.col, -self.row),
            }
            Right => match degrees {
                Ninety => self.rotate(Left, TwoSeventy),
                OneEighty => self.rotate(Left, OneEighty),
                TwoSeventy => self.rotate(Left, Ninety),
            }
        }
    }
}

#[test]
fn rotate() {
    let point = Point::new(10, -4);
    assert_eq!(point.rotate(Left, Ninety), Point::new(4, 10));
    assert_eq!(point.rotate(Left, OneEighty), Point::new(-10, 4));
    assert_eq!(point.rotate(Left, TwoSeventy), Point::new(-4, -10));
    assert_eq!(point.rotate(Right, Ninety), Point::new(-4, -10));
    assert_eq!(point.rotate(Right, OneEighty), Point::new(-10, 4));
    assert_eq!(point.rotate(Right, TwoSeventy), Point::new(4, 10));
}

fn parse_input2(file: &str) -> Result<Instructions2> {
    Ok(Instructions2 {
        instructions: parse_input(file)?.instructions,
        index: 0,
        waypoint: Point::new(-1, 10),
        location: Point::default(),
    })
}

#[test]
fn example2() -> Result<()> {
    let mut instructions = parse_input2("example1")?;
    instructions.run();
    let dist = instructions.location.manhattan(&Point::default());
    assert_eq!(dist, 286);
    Ok(())
}
