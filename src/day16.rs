use adventofcode::*;
use std::collections::{HashSet, HashMap};
use std::ops::RangeInclusive;

fn main() -> Result<()> {
    let ticket_parser = parse_file("a")?;
    answer!("a", ticket_parser.error_rate());
    answer!("b", ticket_parser.departure_product()?);
    Ok(())
}

#[derive(Debug)]
struct TicketParser {
    valid_ranges: ValidRanges,
    my_ticket: Ticket,
    nearby_tickets: Vec<Ticket>,
}

impl TicketParser {
    fn error_rate(&self) -> u32 {
        self.nearby_tickets
            .iter()
            .flat_map(|ticket| ticket.fields.iter())
            .filter(|field| {
                self.valid_ranges
                    .ranges
                    .values()
                    .all(|range| !range.contains(field))
            })
            .sum()
    }

    fn departure_product(&self) -> Result<usize> {
        let indices = self.indices()?;

        Ok(indices.0.iter()
            .filter(|(key, _)| key.starts_with("departure"))
            .map(|(_, val)| self.my_ticket.fields[*val] as usize)
            .product())
    }

    fn possible_indices(&self) -> PossibleIndices {
        PossibleIndices::from(self)
    }

    fn indices(&self) -> Result<Indices> {
        Indices::try_from(self.possible_indices())
    }

    fn valid_tickets(&self) -> Vec<&Ticket> {
        self.nearby_tickets
            .iter()
            .filter(|ticket| {
                ticket.fields.iter().all(|field| {
                    self.valid_ranges
                        .ranges
                        .values()
                        .any(|range| range.contains(field))
                })
            })
            .collect()
    }
}

struct PossibleIndices<'a>(HashMap<&'a str, HashSet<usize>>);

impl<'a> From<&'a TicketParser> for PossibleIndices<'a> {
    fn from(ticket_parser: &'a TicketParser) -> Self {
        let mut map = HashMap::new();
        for i in 0..ticket_parser.valid_ranges.ranges.len() {
            for (key, val) in ticket_parser.valid_ranges.ranges.iter() {
                if ticket_parser.valid_tickets().iter().all(|ticket| val.contains(&ticket.fields[i])) {
                   map.entry(key.as_str()).or_insert(HashSet::new()).insert(i); 
                }
            }
        }

        PossibleIndices(map)
    }
}

struct Indices<'a>(HashMap<&'a str, usize>);

impl<'a> TryFrom<PossibleIndices<'a>> for Indices<'a> {
    type Error = BoxErr;
    fn try_from(possible_indices: PossibleIndices<'a>) -> Result<Self> {
        let mut sorted = possible_indices.0.iter().collect::<Vec<_>>();
        sorted.sort_by(|(_, val_a), (_, val_b)| val_a.len().cmp(&val_b.len()));

        let mut map = HashMap::<&str, usize>::new();
        for (label, indices) in sorted.into_iter() {
            for index in indices.into_iter() {
                if map.values().any(|val| val == index) {
                    continue;
                } else {
                    map.insert(label, *index);
                }
            }
        }

        if map.len() != possible_indices.0.len() {
            err!("could not determine indices")
        } else {
            Ok(Indices(map))
        }
    }
}

#[derive(Debug, Default)]
struct Ticket {
    fields: Vec<u32>,
}

impl FromStr for Ticket {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut ticket = Ticket {
            fields: Vec::new(),
        };

        for field in s.trim().split(',') {
            ticket.fields.push(field.parse()?);
        }

        Ok(ticket)
    }
}

#[derive(Debug)]
struct Ranges([RangeInclusive<u32>; 2]);

impl Ranges {
    fn contains(&self, u: &u32) -> bool {
        self.0.iter().any(|range| range.contains(u))
    }
}

#[derive(Debug, Default)]
struct ValidRanges {
    ranges: HashMap<String, Ranges>,
}

impl FromStr for ValidRanges {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut valid_ranges = ValidRanges {
            ranges: HashMap::new(),
        };

        for line in s.trim().lines() {
            let mut split = line.split(": ");
            let key = split.nth(0).ok_or("range key not found")?.to_string();
            let mut range_split = split.nth(0).ok_or("range values not found")?
                .split(" or ");
            let mut range_a_split = range_split.nth(0).ok_or("range_a not found")?
                .split('-');
            let mut range_b_split = range_split.nth(0).ok_or("range_b not found")?
                .split('-');

            let range_a = range_a_split.nth(0)
                .ok_or("range_a minimum not found")?
                .parse::<u32>()?
                ..=
                range_a_split.nth(0)
                .ok_or("range_a maximum not found")?
                .parse::<u32>()?;

            let range_b = range_b_split.nth(0)
                .ok_or("range_b minimum not found")?
                .parse::<u32>()?
                ..=
                range_b_split.nth(0)
                .ok_or("range_b maximum not found")?
                .parse::<u32>()?;

            let value = Ranges([range_a, range_b]);

            valid_ranges.ranges.insert(key, value);
        }

        Ok(valid_ranges)
    }
}

fn parse_file(file: &str) -> Result<TicketParser> {
    let mut input = String::new();
    input!(file).read_to_string(&mut input)?;
    parse(&input)
}

fn parse(input: &str) -> Result<TicketParser> {
    let mut blocks = input.split("\n\n").filter(|block| !block.is_empty());

    let valid_ranges = blocks.nth(0)
        .ok_or("valid_ranges not found")?
        .parse()?;

    let my_ticket = blocks.nth(0)
        .ok_or("my_ticket not found")?
        .lines()
        .nth(1)
        .ok_or("your ticket")?
        .parse()?;

    let nearby_tickets = blocks.nth(0)
        .ok_or("nearby_tickets not found")?
        .lines()
        .skip(1)
        .filter_map(|line| line.parse::<Ticket>().ok())
        .collect();

    Ok(TicketParser {
        valid_ranges,
        my_ticket,
        nearby_tickets,
    })
}

#[test]
fn example_a() -> Result<()> {
    let ticket_parser = parse(
"class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12"
    )?;

    assert_eq!(ticket_parser.error_rate(), 71);
    Ok(())
}

#[test]
fn example_b() -> Result<()> {
    let ticket_parser = parse(
"class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9"
    )?;

    let indices = ticket_parser.indices()?;
    assert_eq!(indices.0.get("row"), Some(&0));
    assert_eq!(indices.0.get("class"), Some(&1));
    assert_eq!(indices.0.get("seat"), Some(&2));

    Ok(())
}
