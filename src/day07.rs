use adventofcode::*;
use std::collections::HashMap;

fn main() -> Result<()> {
    let rules = input!("a")
        .lines()
        .filter_map(parse::<Rule>)
        .collect::<Rules>();

    let bag = &Bag::new(Shiny, Gold);

    let shiny_gold = rules.can_hold_count(bag);
    answer!("a", shiny_gold);

    let contain_count = rules.must_hold_count(bag);
    answer!("b", contain_count);

    Ok(())
}

#[derive(Debug)]
struct Rules {
    rules: HashMap<Bag, Vec<QtyBag>>,
}

impl Rules {
    fn rules<'a>(&'a self) -> Vec<BorrowedRule<'a>> {
        self.rules
            .keys()
            .filter_map(|bag| self.get_rule(bag))
            .collect()
    }

    fn get_rule<'a>(&'a self, bag: &'a Bag) -> Option<BorrowedRule> {
        Some(BorrowedRule {
            bag,
            contents: self.rules.get(bag)?,
        })
    }

    /// Count the number of bags that can recursively hold a given bag
    fn can_hold_count(&self, match_bag: &Bag) -> usize {
        self.rules()
            .iter()
            .filter(|rule| rule.can_contain(self, match_bag))
            .count()
    }
    
    /// Count the total number of bags a given bag must recursively hold
    fn must_hold_count(&self, match_bag: &Bag) -> usize {
        self.mhc(match_bag) - 1
    }

    fn mhc(&self, match_bag: &Bag) -> usize {
        let rule = self.get_rule(match_bag).unwrap();
        rule.hold_count(self)
    }
}

impl std::iter::FromIterator<Rule> for Rules {
    fn from_iter<I: IntoIterator<Item = Rule>>(iter: I) -> Self {
        Rules {
            rules: iter
                .into_iter()
                .map(|rule| (rule.bag, rule.contents))
                .collect(),
        }
    }
}

#[derive(Debug)]
struct BorrowedRule<'a> {
    bag: &'a Bag,
    contents: &'a Vec<QtyBag>,
}

impl<'a> BorrowedRule<'a> {
    fn can_contain(&self, rules: &Rules, match_bag: &Bag) -> bool {
        self.contents.iter().any(|qtybag| qtybag == match_bag)
            || self
                .rules(rules)
                .iter()
                .any(|rule| rule.can_contain(rules, match_bag))
    }

    fn rules(&self, rules: &'a Rules) -> Vec<BorrowedRule> {
        self.contents
            .iter()
            .filter_map(|qtybag| rules.get_rule(&qtybag.bag))
            .collect()
    }

    fn hold_count(&self, rules: &Rules) -> usize {
        self.contents.iter()
            .fold(1, |acc, qtybag| {
                qtybag.qty as usize * rules.mhc(&qtybag.bag) + acc
            })              
    }
}

impl<'a> From<&'a Rule> for BorrowedRule<'a> {
    fn from(rule: &'a Rule) -> Self {
        BorrowedRule {
            bag: &rule.bag,
            contents: &rule.contents,
        }
    }
}

#[test]
fn can_hold_count() -> Result<()> {
    let rules = input!("example1")
        .lines()
        .filter_map(parse::<Rule>)
        .collect::<Rules>();

    let bag = &Bag::new(Shiny, Gold);

    assert_eq!(rules.can_hold_count(bag), 4);
    Ok(())
}

#[test]
fn must_hold_count() -> Result<()> {
    let bag = &Bag::new(Shiny, Gold);

    let rules1 = input!("example1")
        .lines()
        .filter_map(parse::<Rule>)
        .collect::<Rules>();

    let rules2 = input!("example2")
        .lines()
        .filter_map(parse::<Rule>)
        .collect::<Rules>();

    assert_eq!(rules1.must_hold_count(bag), 32);
    assert_eq!(rules2.must_hold_count(bag), 126);

    Ok(())
}

#[derive(Debug)]
struct Rule {
    bag: Bag,
    contents: Vec<QtyBag>,
}

impl FromStr for Rule {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut split = s.split(" bags contain ");
        let mut bag = split
            .nth(0)
            .ok_or(error!("bag not found"))?
            .split_whitespace();
        let adj = bag.nth(0).ok_or(error!("adjective not found"))?.parse()?;
        let color = bag.nth(0).ok_or(error!("adjective not found"))?.parse()?;
        let bag = Bag { adj, color };
        let contents = split
            .nth(0)
            .ok_or(error!("contents not found"))?
            .split(", ")
            .filter_map(|qtybag| qtybag.parse().ok())
            .collect::<Vec<QtyBag>>();

        Ok(Rule { bag, contents })
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
struct Bag {
    adj: Adjective,
    color: Color,
}

impl Bag {
    fn new(adj: Adjective, color: Color) -> Self {
        Bag { adj, color }
    }
}

impl PartialEq<QtyBag> for Bag {
    fn eq(&self, rhs: &QtyBag) -> bool {
        self == &rhs.bag
    }
}

impl PartialEq<Bag> for QtyBag {
    fn eq(&self, rhs: &Bag) -> bool {
        &self.bag == rhs
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
struct QtyBag {
    qty: u8,
    bag: Bag,
}

impl FromStr for QtyBag {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut split = s.split_whitespace();
        let qty = split.nth(0).ok_or(error!("qty not found"))?.parse()?;
        let adj = split.nth(0).ok_or(error!("adj not found"))?.parse()?;
        let color = split.nth(0).ok_or(error!("color not found"))?.parse()?;
        let bag = Bag { adj, color };

        Ok(QtyBag { qty, bag })
    }
}

use Adjective::*;
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Adjective {
    Bright,
    Clear,
    Dark,
    Dim,
    Dotted,
    Drab,
    Dull,
    Faded,
    Light,
    Mirrored,
    Muted,
    Pale,
    Plaid,
    Posh,
    Shiny,
    Striped,
    Vibrant,
    Wavy,
}

impl FromStr for Adjective {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s {
            "bright" => Bright,
            "clear" => Clear,
            "dark" => Dark,
            "dim" => Dim,
            "dotted" => Dotted,
            "drab" => Drab,
            "dull" => Dull,
            "faded" => Faded,
            "light" => Light,
            "mirrored" => Mirrored,
            "muted" => Muted,
            "pale" => Pale,
            "plaid" => Plaid,
            "posh" => Posh,
            "shiny" => Shiny,
            "striped" => Striped,
            "vibrant" => Vibrant,
            "wavy" => Wavy,
            x => return err!("invalid Adjective: `{}`", x),
        })
    }
}

use Color::*;
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Color {
    Aqua,
    Beige,
    Black,
    Blue,
    Bronze,
    Brown,
    Chartreuse,
    Coral,
    Crimson,
    Cyan,
    Fuchsia,
    Gold,
    Gray,
    Green,
    Indigo,
    Lavender,
    Lime,
    Magenta,
    Maroon,
    Olive,
    Orange,
    Plum,
    Purple,
    Red,
    Salmon,
    Silver,
    Tan,
    Teal,
    Tomato,
    Turquoise,
    Violet,
    White,
    Yellow,
}

impl FromStr for Color {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s {
            "aqua" => Aqua,
            "beige" => Beige,
            "black" => Black,
            "blue" => Blue,
            "bronze" => Bronze,
            "brown" => Brown,
            "chartreuse" => Chartreuse,
            "coral" => Coral,
            "crimson" => Crimson,
            "cyan" => Cyan,
            "fuchsia" => Fuchsia,
            "gold" => Gold,
            "gray" => Gray,
            "green" => Green,
            "indigo" => Indigo,
            "lavender" => Lavender,
            "lime" => Lime,
            "magenta" => Magenta,
            "maroon" => Maroon,
            "olive" => Olive,
            "orange" => Orange,
            "plum" => Plum,
            "purple" => Purple,
            "red" => Red,
            "salmon" => Salmon,
            "silver" => Silver,
            "tan" => Tan,
            "teal" => Teal,
            "tomato" => Tomato,
            "turquoise" => Turquoise,
            "violet" => Violet,
            "white" => White,
            "yellow" => Yellow,
            x => return err!("invalid Color: `{}`", x),
        })
    }
}
