use adventofcode::*;

fn main() -> Result<()> {
    let text = &mut String::new();
    input!("a").read_to_string(text)?;

    let mut passports = Vec::new();
    for set in text.split("\n\n") {
        passports.push(set.parse::<PassBuilder>()?);
    }

    let valid = passports.iter().filter(|pass| pass.is_valid()).count();
    answer!("a", valid);

    let super_valid = passports.into_iter().filter_map(|pass| Passport::try_from(pass).ok()).count();
    answer!("b", super_valid);

    Ok(())
}

#[derive(Debug)]
struct Passport {
    byr: u16,
    iyr: u16,
    eyr: u16,
    hgt: Height,
    hcl: HairColor,
    ecl: EyeColor,
    pid: Pid,
    cid: Option<String>,
}

impl Passport {
    fn validate_byr(byr: u16) -> Result<u16> {
        if byr < 1920 || byr > 2002 {
            err!("birth year out of range: `{}`", byr)
        } else {
            Ok(byr)
        }
    }

    fn validate_iyr(iyr: u16) -> Result<u16> {
        if iyr < 2010 || iyr > 2020 {
            err!("issue year out of range: `{}`", iyr)
        } else {
            Ok(iyr)
        }
    }

    fn validate_eyr(eyr: u16) -> Result<u16> {
        if eyr < 2020 || eyr > 2030 {
            err!("expiration year out of range: `{}`", eyr)
        } else {
            Ok(eyr)
        }
    }
}

impl TryFrom<PassBuilder> for Passport {
    type Error = BoxErr;
    fn try_from(builder: PassBuilder) -> Result<Self> {
        let PassBuilder {byr, iyr, eyr, hgt, hcl, ecl, pid, cid} = builder;

        let byr = Passport::validate_byr(byr.ok_or(error!("byr: None"))?.parse()?)?;
        let iyr = Passport::validate_iyr(iyr.ok_or(error!("iyr: None"))?.parse()?)?;
        let eyr = Passport::validate_eyr(eyr.ok_or(error!("eyr: None"))?.parse()?)?;
        let hgt = hgt.ok_or(error!("hgt: None"))?.parse()?;
        let hcl = hcl.ok_or(error!("hcl: None"))?.parse()?;
        let ecl = ecl.ok_or(error!("ecl: None"))?.parse()?;
        let pid = pid.ok_or(error!("pid: None"))?.parse()?;

        Ok(Passport { byr, iyr, eyr, hgt, hcl, ecl, pid, cid: cid.clone() })
    }
}

#[derive(Debug)]
struct Height {
    value: u8,
    units: Units,
}

impl FromStr for Height {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let value = s.chars()
            .take_while(|c| c.to_string().parse::<u8>().is_ok())
            .collect::<String>();

        let units = s.trim_start_matches(&value).parse()?;
        let value = value.parse::<u8>()?;

        match units {
            Cm => {
                if value < 150 || value > 193 {
                    return err!("height out of range: `{:?}`", s);
                }
            }
            In => {
                if value < 59 || value > 76 {
                    return err!("height out of range: `{:?}`", s);
                }
            }
        }

        Ok(Height {
            value,
            units,
        })
    }
}

#[test]
fn height_from_str() {
    assert!("60in".parse::<Height>().is_ok());
    assert!("190cm".parse::<Height>().is_ok());
    assert!("190in".parse::<Height>().is_err());
    assert!("190".parse::<Height>().is_err());
}

use Units::*;
#[derive(Debug)]
enum Units {
    Cm,
    In,
}

impl FromStr for Units {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s.to_lowercase().trim() {
            "cm" => Cm,
            "in" => In,
            "" => return err!("units not specified"),
            x => return err!("invalid units: {}", x),
        })
    }
}

use EyeColor::*;
#[derive(Debug)]
enum EyeColor {
    Amb,
    Blu,
    Brn,
    Gry,
    Grn,
    Hzl,
    Oth,
}

impl FromStr for EyeColor {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s.to_lowercase().trim() {
            "amb" => Amb,
            "blu" => Blu,
            "brn" => Brn,
            "gry" => Gry,
            "grn" => Grn,
            "hzl" => Hzl,
            "oth" => Oth,
            x => return err!("invalid hair color: `{}`", x),
        })
    }
}

#[test]
fn eye_color_from_str() -> Result<()> {
    assert!("brn".parse::<EyeColor>().is_ok());
    assert!("wat".parse::<EyeColor>().is_err());
    Ok(())
}

#[derive(Debug, PartialEq, Eq)]
struct HairColor {
    color: String
}

impl FromStr for HairColor {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        if let Some(c) = s.get(0..1) {
            if c != "#" {
                return err!("eye color must start with `#`: `{}`", s);
            } else {
                let color = s.get(1..7)
                    .ok_or(Error("hair color value not found".to_string()))?
                    .chars()
                    .take_while(|c| c.to_digit(16).is_some())
                    .collect::<String>();
                
                if color.len() != 6 {
                    return err!("eye color must be 6 hexadecimal digits: `{}`", s);
                }

                Ok(HairColor { color })
            }
        } else {
            err!("cannot parse eye color from empty string")
        }
    }
}

#[test]
fn hair_color_from_str() -> Result<()> {
    assert!("#123abc".parse::<HairColor>().is_ok());
    assert!("#123abz".parse::<HairColor>().is_err());
    assert!("123abc".parse::<HairColor>().is_err());
    assert!("123".parse::<HairColor>().is_err());
    assert!("#123ab".parse::<HairColor>().is_err());
    Ok(())
}

#[derive(Debug)]
struct Pid {
    pid: u32,
}

impl FromStr for Pid {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let digits = s.chars().take_while(|c| format!("{}", c).parse::<u8>().is_ok()).collect::<String>();
        if digits.len() != 9 {
            err!("pid must be 9 digits: `{}`, s")
        } else {
            Ok(Pid {
                pid: digits.parse()?,
            })
        }
    }
}

#[test]
fn pid_from_str() -> Result<()> {
    assert!("000000001".parse::<Pid>().is_ok());
    assert!("00000a001".parse::<Pid>().is_err());
    assert!("0123456789".parse::<Pid>().is_err());
    Ok(())
}

#[derive(Debug, Default, Clone)]
struct PassBuilder {
    /// Birth Year
    byr: Option<String>,
    /// Issued Year
    iyr: Option<String>,
    /// Expiration Year
    eyr: Option<String>,
    /// Height
    hgt: Option<String>,
    /// Hair Color
    hcl: Option<String>,
    /// Eye Color
    ecl: Option<String>,
    /// Passport ID
    pid: Option<String>,
    /// Country ID
    cid: Option<String>,
}

impl PassBuilder {
    fn is_valid(&self) -> bool {
        self.byr.is_some() &&
            self.iyr.is_some() &&
            self.eyr.is_some() &&
            self.hgt.is_some() &&
            self.hcl.is_some() &&
            self.ecl.is_some() &&
            self.pid.is_some()
    }
}

impl FromStr for PassBuilder {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut pass = PassBuilder::default();

        for field in s.split_whitespace() {
            let mut sep = field.split(':');
            if let (Some(key), Some(val)) = (sep.nth(0), sep.nth(0)) {
                match key {
                    "byr" => pass.byr = Some(val.into()),
                    "iyr" => pass.iyr = Some(val.into()),
                    "eyr" => pass.eyr = Some(val.into()),
                    "hgt" => pass.hgt = Some(val.into()),
                    "hcl" => pass.hcl = Some(val.into()),
                    "ecl" => pass.ecl = Some(val.into()),
                    "pid" => pass.pid = Some(val.into()),
                    "cid" => pass.cid = Some(val.into()),
                    _ => return err!("Unknown key: `{}`", key),
                }
            }
        }

        Ok(pass)
    }
}
