use adventofcode::*;
use std::collections::HashSet;

fn main() -> Result<()> {
    let mut groups = String::new();
    input!("a").read_to_string(&mut groups)?;

    let groups = groups.split("\n\n")
        .collect::<Vec<_>>();

    answer!("a", part1(&groups));
    answer!("b", part2(&groups));

    Ok(())
}

fn part1(groups: &Vec<&str>) -> usize {
    groups.iter()
        .map(|group| {
            let mut map = HashSet::new();
            for c in group.chars() {
                if ('a'..='z').contains(&c){
                    map.insert(c);
                }
            }
            map
        })
        .map(|group| group.len())
        .sum()
}

fn part2(groups: &Vec<&str>) -> usize {
    groups.iter()
        .map(|group| {
            let mut set = HashSet::new();
            for line in group.lines() {
                for c in line.chars() {
                    if group.lines().all(|line| line.contains(c)) {
                        set.insert(c);
                    }
                }
            }
            set
        })
        .map(|group| group.len())
        .sum()
}

#[test]
fn example1() -> Result<()> {
    let mut groups = String::new();
    input!("example1").read_to_string(&mut groups)?;
    let groups = groups.split("\n\n")
        .collect::<Vec<_>>();

    let sum = part1(&groups);
    answer!("example1", sum);
    assert_eq!(sum, 11);

    let sum = part2(&groups);
    answer!("example2", sum);
    assert_eq!(sum, 6);

    Ok(())
}
