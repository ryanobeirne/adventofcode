use adventofcode::*;
use std::collections::HashMap;

fn main() -> Result<()> {
    let mut game = parse_input("a")?;
    answer!("a", game.clone().play(2020).0);
    answer!("b", game.play(30_000_000).0);
    Ok(())
}

#[derive(Debug, Clone)]
struct Game {
    map: HashMap<Number, Vec<TurnId>>,
    current_num: Number,
    current_turn: TurnId,
}

impl Game {
    fn insert(&mut self, number: Number, turn: TurnId) {
        self.map.entry(number).or_insert(Vec::new()).push(turn)
    }

    fn play(&mut self, rounds: usize) -> Number {
        while self.current_turn.0 < rounds {
            self.round();
        }

        self.current_num
    }

    fn round(&mut self) {
        self.increment();
        self.insert(self.current_num, self.current_turn);
    }

    fn increment(&mut self) {
        self.current_num = self.next_num();
        self.current_turn.0 += 1;
    }

    fn next_num(&self) -> Number {
        Number(
            if let Some(turns) = self.map.get(&self.current_num) {
                match turns.len() {
                    0 => 0,
                    1 => self.current_turn.0 - turns[0].0,
                    x => self.current_turn.0 - turns[x-2].0,
                }
            } else {
                0
            }
        )
    }

}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Number(usize);

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct TurnId(usize);

fn parse_input(file: &str) -> Result<Game> {
    let mut input = String::new();
    input!(file).read_to_string(&mut input)?;
    parse(&input)
}

fn parse(input: &str) -> Result<Game> {
    let mut game = Game{
        map: HashMap::new(),
        current_num: Number(0),
        current_turn: TurnId(0),
    };

    for num in input.trim().split(',') {
        game.current_turn.0 += 1;
        game.current_num = Number(num.parse()?);
        game.insert(game.current_num, game.current_turn);
    }

    Ok(game)
}

macro_rules! examples {
    ($n:ident, $game:expr, $answer:expr) => {
        #[test]
        fn $n() -> Result<()> {
            example_2020($game, $answer)
        }
    }
}

#[cfg(test)]
fn example_2020(game: &str, answer: usize) -> Result<()> {
    assert_eq!(parse(game)?.play(2020).0, answer);
    Ok(())
}

examples!(example_0, "0,3,6", 436);
examples!(example_a, "1,3,2", 1);
examples!(example_b, "2,1,3", 10);
examples!(example_c, "1,2,3", 27);
examples!(example_d, "2,3,1", 78);
examples!(example_e, "3,2,1", 438);
examples!(example_f, "3,1,2", 1836);

#[test]
fn example_main() -> Result<()> {
    let mut game = parse("0,3,6")?;
    assert_eq!(game.current_turn, TurnId(3), "derp");
    assert_eq!(game.current_num, Number(6), "derp");
    game.round();
    assert_eq!(game.current_turn, TurnId(4));
    assert_eq!(game.current_num, Number(0));
    game.round();
    assert_eq!(game.current_turn, TurnId(5));
    assert_eq!(game.current_num, Number(3));

    Ok(())
}
