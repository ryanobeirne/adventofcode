pub use std::{
    convert::{TryFrom, TryInto},
    fmt,
    fs::File,
    io::{Read, BufRead, BufReader, Write, BufWriter},
    path::{Path, PathBuf},
    str::FromStr,
};

#[macro_export]
macro_rules! input {
    ($abc:expr) => {
        BufReader::new(
            File::open(
                PathBuf::new()
                    .join(env!("CARGO_MANIFEST_DIR"))
                    .join("src")
                    .join("input")
                    .join(format!("{}.{}.txt", env!("CARGO_BIN_NAME"), $abc))
            )?
        )
    };
}

pub fn parse<T: FromStr>(entry: std::io::Result<String>) -> Option<T> {
    Some(entry.ok()?.trim().parse::<T>().ok()?)
}

#[macro_export]
macro_rules! answer {
    ($abc:expr, $answer:expr) => {
        println!("{}-{}: {}", env!("CARGO_BIN_NAME"), $abc, $answer);
    }
}

#[macro_export]
macro_rules! error {
    ($error:expr) => {
        Error($error.into())
    };
    ($fmt:tt, $($val:expr),+) => {
        error!(format!($fmt, $($val),*))
    };
}

#[macro_export]
macro_rules! err {
    ($error:tt) => {
        Err(Box::new(error!($error)))
    };
    ($fmt:tt, $($val:expr),+) => {
        Err(Box::new(error!(format!($fmt, $($val),*))))
    };
}

pub struct Error(pub String);

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

pub type BoxErr = Box<dyn std::error::Error>;
pub type Result<T> = std::result::Result<T, BoxErr>;
