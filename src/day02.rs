use adventofcode::*;

fn main() -> Result<()> {
    let input = input!("a")
        .lines()
        .filter_map(parse::<PolicyPass>)
        .collect::<Vec<_>>();

    let count_a = input.iter()
        .filter(|pp| pp.is_valid())
        .count();

    answer!("a", count_a);

    let count_b = input.iter()
        .filter(|pp| pp.is_valid_2())
        .count();

    answer!("b", count_b);

    Ok(())
}

#[derive(Debug)]
struct PolicyPass {
    low: usize,
    high: usize,
    letter: char,
    password: String,
}

impl PolicyPass {
    fn is_valid(&self) -> bool {
        (self.low..=self.high).contains(&self.char_count())
    }

    fn is_valid_2(&self) -> bool {
        let mut count = 0;

        let chars = self.password.chars().collect::<Vec<_>>();

        if let Some(first) = chars.get(self.low - 1) {
            if first == &self.letter {
                count += 1;
            }
        }

        if let Some(second) = chars.get(self.high - 1) {
            if second == &self.letter {
                count += 1;
            }
        }

        count == 1
    }

    fn char_count(&self) -> usize {
        self.password.chars()
            .filter(|c| c == &self.letter)
            .count()
    }
}

impl FromStr for PolicyPass {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let split = s.split(' ').collect::<Vec<&str>>();

        let range = split[0].split('-')
            .filter_map(|s| s.parse::<usize>().ok())
            .collect::<Vec<_>>();

        let low = *range.get(0).ok_or_else(|| "low range not found")?;
        let high = *range.get(1).ok_or_else(|| "high range not found")?;

        let letter = split.get(1).ok_or_else(|| "letter not found")?
            .trim_end_matches(':')
            .chars()
            .nth(0)
            .ok_or_else(|| "letter not found")?;

        let password = split.get(2).ok_or_else(|| "password not found")?.trim().into();

        Ok(PolicyPass {
            low,
            high,
            letter,
            password,
        })
    }
}
