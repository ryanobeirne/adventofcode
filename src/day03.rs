use adventofcode::*;

const SLOPE: Point = Point::new(3, 1);
const SLOPES: &[Point] = &[
    Point::new(1,1),
    SLOPE,
    Point::new(5,1),
    Point::new(7,1),
    Point::new(1,2),
];

fn main() -> Result<()> {
    let map = input!("a").lines().filter_map(parse::<Row>).collect::<Map>();

    answer!("a", map.count_trees(&SLOPE));
    answer!("b", map.count_trees_2());

    Ok(())
}

#[derive(Debug)]
struct Map {
    rows: Vec<Row>,
}

impl Map {
    fn get(&self, point: &Point) -> Option<&Square> {
        self.rows.get(point.y)?.get(point.x)
    }

    fn count_trees(&self, slope: &Point) -> usize {
        let mut head = Point::zero();
        let mut count = 0;

        while let Some(square) = self.get(&head) {
            head.x += slope.x;
            head.y += slope.y;
            if square == &Tree {
                count += 1;
            }
        }

        count
    }

    fn count_trees_2(&self) -> usize {
        SLOPES.iter().fold(1, |acc, slope| acc * self.count_trees(slope))
    }
}

impl std::iter::FromIterator<Row> for Map {
    fn from_iter<I: IntoIterator<Item = Row>>(iter: I) -> Self {
        Map {
            rows: iter.into_iter().collect(),
        }
    }
}

#[derive(Debug)]
struct Row {
    squares: Vec<Square>,
}

impl Row {
    fn get(&self, index: usize) -> Option<&Square> {
        self.squares.get(index % self.squares.len())
    }
}

impl FromStr for Row {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut squares = Vec::new();

        for c in s.trim().chars() {
            squares.push(c.try_into()?)
        }

        Ok(Row { squares })
    }
}

use Square::*;
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Square {
    Tree,
    Empty,
}

impl fmt::Display for Square {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            Tree => '#',
            Empty => '.',
        })
    }
}

impl FromStr for Square {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        if s.trim().len() != 1 {
            return err!("&str must be only 1 character")
        }

        s.trim().chars().nth(0).ok_or(error!("square is empty string"))?.try_into()
    }
}

impl TryFrom<char> for Square {
    type Error = BoxErr;
    fn try_from(c: char) -> Result<Self> {
        Ok(match c {
            '#' => Tree,
            '.' => Empty,
            x => return err!("invalid square character: `{}`", x),
        })
    }
}

#[derive(Debug)]
struct Point {
    x: usize,
    y: usize,
}

impl Point {
    const fn new(x: usize, y: usize) -> Self {
        Point {x,y}
    }
    const fn zero() -> Self {
        Point::new(0,0)
    }
}

#[test]
fn example_a() -> Result<()> {
    let map = input!("example_a")
        .lines()
        .filter_map(parse::<Row>)
        .collect::<Map>();

    assert_eq!(map.get(&Point::new(3,1)), Some(&Empty));
    assert_eq!(map.get(&Point::new(13,0)), Some(&Tree));
    assert_eq!(map.get(&Point::new(13,1)), Some(&Empty));
    assert_eq!(map.get(&Point::new(13,2)), Some(&Empty));
    assert_eq!(map.get(&Point::new(13,3)), Some(&Tree));
    assert_eq!(map.get(&Point::new(25,0)), Some(&Tree));

    assert_eq!(map.count_trees(&SLOPE), 7);

    Ok(())
}

#[test]
fn example_b() -> Result<()> {
    let map = input!("example_a")
        .lines()
        .filter_map(parse::<Row>)
        .collect::<Map>();

    let trees = map.count_trees_2();

    assert_eq!(trees, 336);

    Ok(())
}
