use adventofcode::*;

fn main() -> Result<()> {
    let entries = input!("a").lines().filter_map(parse).collect::<Vec<_>>();

    answer!("a", part_a(&entries)?);
    answer!("b", part_b(&entries)?);

    Ok(())
}

fn part_a(entries: &Vec<u32>) -> Result<u32> {
    for i in entries.iter() {
        for j in entries.iter() {
            if i + j == 2020 {
                return Ok(i * j);
            }
        }
    }

    err!("try again, doofus")
}

fn part_b(entries: &Vec<u32>) -> Result<u32> {
    for i in entries.iter() {
        for j in entries.iter() {
            for k in entries.iter() {
                if i + j + k == 2020 {
                    return Ok(i * j * k);
                }
            }
        }
    }

    err!("try again, doofus")
}
