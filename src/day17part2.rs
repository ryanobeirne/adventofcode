use adventofcode::*;
use std::collections::HashMap;
use super::*;

#[derive(Debug)]
pub struct Pocket4d {
    grid: HashMap<Point4d, State>,
}

impl Pocket4d {
    pub fn run(&mut self, cycles: usize) {
        for _ in 0..cycles {
            self.cycle();
        }
    }

    fn cycle(&mut self) {
        let Point4d { w: min_w, x: min_x, y: min_y, z: min_z } = self.min();
        let Point4d { w: max_w, x: max_x, y: max_y, z: max_z } = self.max();

        let mut flips = Vec::new();

        for w in min_w..=max_w {
            for x in min_x..=max_x {
                for y in min_y..=max_y {
                    for z in min_z..=max_z {
                        let point = Point4d::new(w, x, y, z);
                        let neighbors = point.neighbors();
                        let active_count = neighbors.iter().filter(|n| self.grid.get(&n) == Some(&Active)).count();
                        match (self.grid.get(&point).unwrap_or_default(), active_count) {
                            (Active, 2) | (Active, 3) => (),
                            (Active, _) => {
                                flips.push(point);
                            }
                            (Inactive, 3) => {
                                flips.push(point);
                            }
                            _ => (),
                        }
                    }
                }
            }
        }

        for flip in flips {
            self.grid.entry(flip).or_default().flip();
        }
    }

    pub fn active_count(&self) -> usize {
        self.grid.values().filter(|state| state == &&Active).count()
    }

    fn min(&self) -> Point4d {
        Point4d::new(
            self.grid.keys().min_by(|a, b| a.w.cmp(&b.w)).unwrap().w - 1,
            self.grid.keys().min_by(|a, b| a.x.cmp(&b.x)).unwrap().x - 1,
            self.grid.keys().min_by(|a, b| a.y.cmp(&b.y)).unwrap().y - 1,
            self.grid.keys().min_by(|a, b| a.z.cmp(&b.z)).unwrap().z - 1,
        )
    }

    fn max(&self) -> Point4d {
        Point4d::new(
            self.grid.keys().max_by(|a, b| a.w.cmp(&b.w)).unwrap().w + 1,
            self.grid.keys().max_by(|a, b| a.x.cmp(&b.x)).unwrap().x + 1,
            self.grid.keys().max_by(|a, b| a.y.cmp(&b.y)).unwrap().y + 1,
            self.grid.keys().max_by(|a, b| a.z.cmp(&b.z)).unwrap().z + 1,
        )
    }
}

impl FromStr for Pocket4d {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut grid = HashMap::new();

        for (y, line) in s.lines().enumerate() {
            for (x, ch) in line.chars().enumerate() {
                grid.insert(
                    Point4d::new(0, x as i32, y as i32, 0),
                    ch.to_string().parse()?,
                );
            }
        }

        Ok(Pocket4d { grid })
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Point4d {
    w: i32,
    x: i32,
    y: i32,
    z: i32,
}

impl Point4d {
    fn new(w: i32, x: i32, y: i32, z: i32) -> Self {
        Point4d {w, x, y, z}
    }

    fn neighbors(&self) -> Vec<Self> {
        let mut neighbors = Vec::new();

        let w_p = self.w + 1;
        let w_n = self.w - 1;
        let x_p = self.x + 1;
        let x_n = self.x - 1;
        let y_p = self.y + 1;
        let y_n = self.y - 1;
        let z_p = self.z + 1;
        let z_n = self.z - 1;

        for w in w_n..=w_p {
            for x in x_n..=x_p {
                for y in y_n..=y_p {
                    for z in z_n..=z_p {
                        let point = Point4d::new(w, x, y, z);
                        if &point != self {
                            neighbors.push(point);
                        }
                    }
                }
            }
        }

        neighbors
    }
}

pub fn parse_input_4d(file: &str) -> Result<Pocket4d> {
    let input = &mut String::new();
    input!(file).read_to_string(input)?;
    input.parse()
}
