use adventofcode::*;
use std::collections::HashMap;

fn main() -> Result<()> {
    let mut bootcode = input!("a").lines().filter_map(parse::<Instruction>).collect::<BootCode>();
    let bootcode2 = bootcode.clone();

    bootcode.run();
    answer!("a", bootcode.accumulator);

    for i in 0..bootcode2.instructions.len() {
        let mut bootcode = bootcode2.clone().flip(i);
        if let Some(acc) = bootcode.run() {
            answer!("b", acc);
            break;
        }
    }

    Ok(())
}

#[derive(Debug, Clone)]
struct BootCode {
    instructions: Vec<Instruction>,
    accumulator: i32,
    index: usize,
}

impl BootCode {
    fn run(&mut self) -> Option<i32> {
        let mut counter = HashMap::new();

        while let Some(_instruction) = self.next() {
            *counter.entry(self.index).or_insert(0) += 1;
            if let Some(count) = counter.get(&self.index) {
                if count > &1 {
                    return None;
                }
            }
        }

        Some(self.accumulator)
    }

    fn next(&mut self) -> Option<&Instruction> {
        match self.instructions.get(self.index)? {
            Acc(acc) => {
                self.accumulator += acc;
                self.index += 1;
            }
            Jmp(jmp) => {
                if jmp.is_negative() {
                    self.index -= jmp.abs() as usize;
                } else {
                    self.index += jmp.abs() as usize;
                }
            }
            Nop(_) => {
                self.index += 1;
            }
        }

        self.instructions.get(self.index)
    }

    fn flip(mut self, index: usize) -> Self {
        if let Some(instruction) = self.instructions.get_mut(index) {
            *instruction = instruction.flip();
        }
        self
    }
}

impl std::iter::FromIterator<Instruction> for BootCode {
    fn from_iter<I: IntoIterator<Item=Instruction>>(iter: I) -> Self {
        BootCode {
            instructions: iter.into_iter().collect(),
            accumulator: 0,
            index: 0,
        }
    }
}

use Instruction::*;
#[derive(Debug, Copy, Clone)]
enum Instruction {
    Acc(i32),
    Jmp(i32),
    Nop(i32),
}

impl Instruction {
    fn flip(&self) -> Self {
        match self {
            Acc(i) => Acc(*i),
            Jmp(i) => Nop(*i),
            Nop(i) => Jmp(*i),
        }
    }
}

impl FromStr for Instruction {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s.get(0..3).ok_or(error!("instruction not found"))? {
            "acc" => Acc(s.get(4..).ok_or(error!("acc instruction value not found"))?.parse()?),
            "jmp" => Jmp(s.get(4..).ok_or(error!("jmp instruction value not found"))?.parse()?),
            "nop" => Nop(s.get(4..).ok_or(error!("nop instruction value not found"))?.parse()?),
            x => return err!("invalid instruction: `{}`", x),
        })
    }
}
